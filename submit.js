const form = document.querySelector("#myForm");
const statusSubmit = document.querySelector(".status-submit");

const dataChangeForm = (form) => {
  const data = new FormData(form);
  data.delete('hiddenInput');
  data.append('newField', 'customValue');
  submit(data);
};

const submit = async (dataForm) => {
  await fetch("/posturl/", {
    method: "POST",
    body: dataForm,
  });
};

form.onsubmit = (e) => {
  e.preventDefault();
  dataChangeForm(form);
  statusSubmit.style.display = "inline-block";
};
